package util
package db

import scalaprops._
import prelude._, std._, test._

import data.Data
import fsm.FID
import net.{Code, Response}, net.netty.http, net.netty.crypto.Bits

import db.instances.data._
import fsm.instances.data.DataFID
import std.instances.data._
import net.instances.data.DataResponse

import Blob._

object ServiceSpec extends Scalaprops {

  val Address = net.netty.Address("localhost", 8888)
  val Store   = fsm.Store.Memory[Blob[Int], Message[Int], Action]("db")

  object Tab       extends io.device.Table("index")
  object KeyIndex  extends Tab.Fragment[Bits, FID]("key")
  object BlobIndex extends Tab.Fragment[FID,  List[FID]]("blob")

  object MyEngine  extends FSM.Engine[Int]("my", Context(KeyIndex, BlobIndex), Store)
  object MyServer0 extends Server[Int]("my", MyEngine, Context(KeyIndex, BlobIndex))
  object MyServer  extends http.Server(Address, MyServer0) //, packetDump = false)
  object MyClient0 extends http.Client(Address)
  object MyClient  extends Client[Int]("my", MyClient0) //, packetDump = false)

  def unsafeUnwrap[A: Data, B: Data](rep: Response[A, B]): B = rep match {
    case Response(Code.Ok(Just(b)), _) => b
    case rep => std.unsafe.abort("Response.unsafeUnwrap", rep) }

//======================================================================
  val endToEndTest = Property.forAll {

    val bits1  = sec.Key.unpadded[Int]().secret
    val bits2  = sec.Key.unpadded[Int]().secret
    val owner1 = FID("1")
    val owner2 = FID("2")

    val partial1 = PartialObj(1.just, "one".just, List("a").just)
    val partial2 = PartialObj(2.just, "two".just, List("b").just)
    val partial3 = PartialObj(3.just, "three".just, List("a", "b").just)

    val partial4 = PartialObj(value=4.just)
    val partial5 = PartialObj[Int](label="four".just)
    val partial6 = PartialObj[Int](tags=nil.just)

    val task = for {

      _     <- MyEngine.init
      _     <- MyServer.start & Entry.nop

      _     <- KeyIndex.insert(bits1, owner1)
      _     <- KeyIndex.insert(bits2, owner2)

      blob1 <- MyClient.post(bits1, partial1)()
      blob2 <- MyClient.post(bits1, partial2)()
      blob3 <- MyClient.post(bits1, partial3)()

      rep1  <- MyClient.put(bits1, unsafeUnwrap(blob1).getKey, partial4)()
      rep2  <- MyClient.put(bits1, unsafeUnwrap(blob2).getKey, partial5)()
      rep3  <- MyClient.put(bits1, unsafeUnwrap(blob3).getKey, partial6)()

      _     <- MyClient.delete(bits1, unsafeUnwrap(blob1).getKey)()

      rep4  <- MyClient.get(bits1, unsafeUnwrap(blob1).getKey)()
      rep5  <- MyClient.get(bits1, unsafeUnwrap(blob2).getKey)()
      rep6  <- MyClient.get(bits1, FID("xyz"))()
      rep7  <- MyClient.get(bits2, unsafeUnwrap(blob1).getKey)()

      rep8  <- MyClient.multiget(bits1)()
      rep9  <- MyClient.multiget(bits2)()

      _     <- MyServer.stop & Entry.nop

    } yield (rep1, rep2, rep3, rep4, rep5, rep6, rep7, rep8, rep9)

    val res @ \/-((rep1, rep2, rep3, rep4, rep5, rep6, rep7, rep8, rep9)) =
      unsafeRun(task)

    //unsafe.println(res)

    rep1 mustMatch { case Response(Code.Ok(Just(Obj(_, 4, "one", _, "my", _, _, _))),  _) => true }
    rep2 mustMatch { case Response(Code.Ok(Just(Obj(_, 2, "four", _, _, _, _, _))), _) => true }
    rep3 mustMatch { case Response(Code.Ok(Just(Obj(_, 3, _, Nil(), _, _, _, _))),  _) => true }

    rep4 mustMatch { case Response(Code.NotAuthorized(_), _) => true }
    rep5 mustMatch { case Response(Code.Ok(Just(Obj(_, 2, "four", _, _, _, _, _))), _) => true }
    rep6 mustMatch { case Response(Code.NotAuthorized(_), _) => true }
    rep7 mustMatch { case Response(Code.NotAuthorized(_), _) => true }

    rep8 mustMatch { case Response(Code.Ok(Just(objs)), _) =>
      objs.map(_.getValue).sorted == List(2, 3) }
    rep9 mustMatch { case Response(Code.Ok(Just(Nil())), _) => true }

  }

} //ServiceSpec
