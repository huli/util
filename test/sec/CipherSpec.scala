package util
package sec

import scalaprops._
import prelude._, std._, test._

object CipherSpec extends Scalaprops {

  val pkTest = Property.forAll {

    val aes = new AES("mykey", "mysalt")
    val str = "foobarbaz"
    val enc = aes encrypt str
    val dec = aes decrypt enc

    str mustEqual dec
  }

} //CipherSpec
