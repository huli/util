package util
package sec

import scalaprops._
import prelude._, std._, test._

object KeySpec extends Scalaprops {

  val basicTest = Property.forAll {

    val key = Key[String]()

    key.active mustEqual false
    key.live   mustEqual false
  }

} //KeySpec
