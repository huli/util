package util
package sec

import scalaprops._
import prelude._, std._, test._

object CryptoSpec extends Scalaprops {

  val scryptTest = Property.forAll { (x1: Long, x2: Long) =>

    crypto.scrypt(x1.toString) == crypto.scrypt(x2.toString) mustEqual false
  }

} //CryptoSpec
