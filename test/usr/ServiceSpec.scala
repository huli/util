package util
package usr

import scalaprops._

import prelude._, std._, test._
import net.netty.{ Address => Addr, http }, net.netty.crypto.Bits
import io.device.Table

import std.instances.all._
import fsm.instances.data.DataFID

import db.instances.data.DataBits
import usr.instances.data._

object Types {
  type Entity   = Int
  type Settings = Int
}
import Types._

object Cfg extends svc.Env {
  lazy val API_KEY_POSTMARK = getString("API_KEY_POSTMARK")
  lazy val MAIL_DOMAIN      = getString("MAIL_DOMAIN")
  lazy val MAIL_FROM        = getString("MAIL_FROM")
  lazy val MAIL_TO          = getString("MAIL_TO")
}

object Ctx {
  val address = Addr("localhost", 8888)

  val store = fsm.Store.Memory[
    User[Entity, Settings],
    Message[Entity, Settings],
    Action[Entity, Settings]]("users")

  val defaults = 42

  val welcome = (e: Address, t: Token) => mail.Mail(from, e.get, "welcome", t.get)
  val verify  = (e: Address, t: Token) => mail.Mail(from, e.get, "verify", t.get)
  val reset   = (e: Address, t: Token) => mail.Mail(from, e.get, "reset", t.get)
  val from    = s"${Cfg.MAIL_FROM}@${Cfg.MAIL_DOMAIN}"
  val smtpd   = new mail.Postmark(Cfg.API_KEY_POSTMARK)

  val tab        = new Table("index")
  val emailIndex = new tab.Fragment[Address, fsm.FID]("email")
  val keyIndex   = new tab.Fragment[Bits, fsm.FID]("key")

  val cipher = new sec.AES("key", "salt")

  val get = Context(
    List(666),
    defaults,
    (uid: fsm.FID, user: User[Entity, Settings]) => Task0 {
      val msg: Message[Entity, Settings] = Message.PutSettings(user.settings/2)
      fsm.Msg(msg).just
    } & Entry.nop,
    welcome, verify, reset, smtpd,
    keyIndex, emailIndex,
    cipher )
}

object MyEngine  extends FSM.Engine[Entity, Settings](Ctx.get, Ctx.store)
object MyServer0 extends Server[Entity, Settings](MyEngine, Ctx.get)
object MyServer  extends http.Server(Ctx.address, MyServer0)
object MyClient  extends Client[Entity, Settings](new http.Client(Ctx.address))

//======================================================================
object ServiceSpec extends Scalaprops {

  val endToEndTest = Property.forAll {

    val u  = s"${Cfg.MAIL_TO}@${Cfg.MAIL_DOMAIN}"
    val p  = "xyzzy"
    val p2 = "yzzyx"

    def unwrap[A, B](rep: net.Response[A, B]): B = rep match {
      case net.Response(net.Code.Ok(Just(b)), _) => b
      case rep => unsafe.abort("net.unwrap", rep.toString) }

    val task = for {
      _         <- MyEngine.init
      _         <- MyServer.start & Entry.nop

      rtoken    <- MyClient.register(u, p)()
      token     = unwrap(rtoken)
      rkey      <- MyClient.authenticate(token)()
      bits      = unwrap(rkey)._1

      rep1      <- MyClient.getSettings(bits)()
      _         <- MyClient.logout(bits)()
      rep2      <- MyClient.authenticate(token)() //token expired
      rep3      <- MyClient.getSettings(bits)()   //key still works

      rtoken2   <- MyClient.login(u, p)()
      token2    = unwrap(rtoken2)
      rkey2     <- MyClient.authenticate(token2)()
      bits2     = unwrap(rkey2)._1
      rep4      <- MyClient.putSettings(bits2, 43)()

      _         <- MyClient.requestVerify(bits)()
      tok       <- MyEngine.store.fsm.scan map { rs =>
        rs.headMaybe             |>
        unsafe.unwrap            |>
        (_._2.state.email.token) |>
        unsafe.unwrap }
      _         <- MyClient.performVerify(tok)()
      rep5      <- MyClient.performVerify(tok)()


      _         <- MyClient.requestReset(u)()
      tok2      <- MyEngine.store.fsm.scan map { rs =>
        rs.headMaybe                   |>
        unsafe.unwrap                  |>
        (_._2.state.credentials.token) |>
        unsafe.unwrap }
      _         <- MyClient.performReset(tok2, p2)()
      rtoken3   <- MyClient.login(u, p2)()
      token3    = unwrap(rtoken3)
      rep6      <- MyClient.authenticate(token3)()
      rep7      <- MyClient.getUser(bits2)()

      _         <- MyServer.stop & Entry.nop
    } yield (rep1, rep2, rep3, rep4, rep5, rep6, rep7)

    val res @ \/-((rep1, rep2, rep3, rep4, rep5, rep6, rep7)) = unsafeRun(task)

    rep1 mustMatch { case net.Response(net.Code.Ok(Just(21)),   _) => true }
    rep2 mustMatch { case net.Response(net.Code.Crash(Empty()), _) => true }
    rep3 mustMatch { case net.Response(net.Code.Ok(Just(21)),   _) => true }

    rep4 mustMatch { case net.Response(net.Code.Ok(Just(43)),   _) => true }
    rep5 mustMatch { case net.Response(net.Code.Ok(Just(())),   _) => true }
    rep6 mustMatch { case net.Response(net.Code.Ok(Just(_)),    _) => true }
    rep7 mustMatch { case net.Response(net.Code.Ok(Just(_)),    _) => true }

  }.toProperties(
    "endToEnd",
    Param.timeout(1, java.util.concurrent.TimeUnit.MINUTES)
  )

} //ServiceSpec
