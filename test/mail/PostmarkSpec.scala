package util
package mail

import scalaprops._
import prelude._, std._, test._

object PostmarkSpec extends Scalaprops {

  object Cfg extends svc.Env {
    lazy val MAIL_DOMAIN = getString("MAIL_DOMAIN")
    lazy val MAIL_FROM   = getString("MAIL_FROM")
    lazy val MAIL_TO     = getString("MAIL_TO")
    lazy val MAIL_BCC    = getString("MAIL_BCC")
    lazy val API_KEY     = getString("API_KEY_POSTMARK")
  }

  val pm = new Postmark(Cfg.API_KEY)

  val mail = Mail(
    s"${Cfg.MAIL_FROM}@${Cfg.MAIL_DOMAIN}",
    s"${Cfg.MAIL_TO}@${Cfg.MAIL_DOMAIN}",
    s"${Cfg.MAIL_BCC}@${Cfg.MAIL_DOMAIN}",
    "Test",
    "testing",
    Body.Text("hihi"))

  val mail2 = mail.copy(to="foo")

//======================================================================

  val okTest = Property.forAll {
    val res = unsafeRunQuiet( pm.send(mail) )
    res.isRight mustEqual true
  }

  val errorTest = Property.forAll {
    val res = unsafeRunQuiet( pm.send(mail2) )
    res.isLeft mustEqual true
  }

} //PostmarkSpec
