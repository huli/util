package util
package geo

import scalaprops._
import prelude._, std._, test._

object IpstackSpec extends Scalaprops {

  object Cfg extends svc.Env {
    lazy val API_KEY = getString("API_KEY_IPSTACK")
  }

  val ipstack = new Ipstack(Cfg.API_KEY)

  val okTest = Property.forAll {
    val res = unsafeRunQuiet( ipstack.lookup("8.8.8.8") )
    res.isRight mustEqual true
  }

  val errorTest = Property.forAll {
    val res = unsafeRunQuiet( ipstack.lookup("foobar") )
    res.isLeft mustEqual true
  }

} //IpstackSpec
