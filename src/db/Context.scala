package util
package db

import prelude._, std._
import fsm.FID
import net.netty.crypto.Bits

final case class Context
  (keyIdx : io.Fragment[Bits, FID],       //key  -> user
   blobIdx: io.Fragment[FID,  List[FID]]) //user -> blobs

// Context
