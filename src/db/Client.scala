package util
package db

import prelude._, std._
import data.Data
import fsm.FID
import net._, dsl._, netty.crypto.Bits

import usr.Token
import Blob.PartialObj

import db.instances.data._
import std.instances.data._
import fsm.instances.data.DataFID

class Client[A: Data](name: String, prim: net.PrimCall) extends net.Client(prim)
{
  def id2str(id: FID): String = Data[FID].toDatum(id).string.get

  def post(key: Bits, partial: PartialObj[A]) =
    Call [PartialObj[A], Unit, Blob[A]] (Post upon Root / name)
      .header(AuthHeader(key))
      .body(partial)

  def put(key: Bits, id: FID, partial: PartialObj[A]) =
    Call [PartialObj[A], Unit, Blob[A]] (Put upon Root / name / id2str(id))
      .header(AuthHeader(key))
      .body(partial)

  def delete(key: Bits, id: FID) =
    Call [Unit, Unit, Unit] (Delete upon Root / name / id2str(id))
      .header(AuthHeader(key))

  def get(key: Bits, id: FID) =
    Call [Unit, Unit, Blob[A]] (Get upon Root / name / id2str(id))
      .header(AuthHeader(key))

  def multiget(key: Bits) =
    Call [Unit, Unit, List[Blob[A]]] (Get upon Root / name)
      .header(AuthHeader(key))

} //Client
