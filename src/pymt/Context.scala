package util
package pymt

import prelude._, std._
import fsm.FID
import net.netty.crypto.Bits

import usr.Address
import mail.{ Mail, Send }

final case class Context
  [Entity]
  (plans                   : Map[String, List[Entity]],
   users                   : usr.FSM.Engine[Entity, _],

   keyIdx                  : io.Fragment[Bits, FID],       //key -> user
   subscriptionIdx         : io.Fragment[FID,  List[FID]], //user -> subscriptions

   stripe                  : Stripe,

   trialStartMail          : Address => Mail,
   trialEndMail            : Address => Mail,
   paymentFailedMail       : Address => Mail,
   subscriptionCanceledMail: Address => Mail,
   smtpd                   : Send)

// Context
