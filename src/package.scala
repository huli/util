package object util {
  import prelude._, std._
  import data.Data
  import net.{ Request, Stringly }, net.netty.crypto.Bits
  import net.instances.data.DataStringly

  type Body = Map[String, String]

  def AuthHeader(key: Bits) = Stringly(XApiKey, key.get)

  // FIXME: Base64 uses 1-2 = as padding...
  def getAuthHeader(req: Request[_]): Maybe[Bits] = req
    .headers
    .getCustom
    .find(_.name.toLowerCase == XApiKey.toLowerCase)
    .map {
      case Stringly(_, v, _) if v != ""      => v
      case Stringly(_, _, (k->Nil()::Nil())) => s"${k}="
      case x                                 => unsafe.abort("AuthHeader", x) }
    .map(Bits.unsafeCertify)

  val XApiKey = "X-Api-Key"

  def body[A: Data](req: Request[A])(implicit ev: ClassTag[A]): Maybe[A] =
    req.body match {
      case just @ Just(_)                 => just
      case Empty() if ev == ClassTag.Unit => unit.just.asInstanceOf[Maybe[A]]
      case empty                          => empty }

} //package
