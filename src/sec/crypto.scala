package util
package sec

import org.bouncycastle.crypto.generators.SCrypt

import prelude._, std._
import net.netty.crypto.{ Bits, asciiArmor0 }

object crypto {

  def scrypt(pw: String): (String, String) = {
    val s = salt()
    val h = scrypt(pw, s)
    (h, s)
  }

  def salt(): String = Bits(64).get

  def scrypt(pw: String, salt: String): String =
    SCrypt.generate(
      pw.getBytes,   //passphrase
      salt.getBytes, //salt
      1 << 14,       //N: CPU/memory cost
      8,             //r: block size
      1,             //p: parallelization
      64             //len: key length
    ) |> asciiArmor0 |> (new String(_))

} //crypto
