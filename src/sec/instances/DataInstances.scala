package util
package sec
package instances

import prelude._, std._, data._
import typ.hlist._, typ.nat._
import std.instances.data._
import net.netty.crypto.Bits
import svc.Service

trait DataInstances {

  implicit def DataBits: Data[Bits] = Data.instance(
    bits => DString(bits.get),
    d => d.asString |> Bits.unsafeCertify )

  implicit def DataMap[A: Data: Ordered, B: Data]: Data[Map[A, B]] =
    Data.instance(
      m => m.toIList |> Data[List[A -> B]].toDatum,
      d => Data[List[A -> B]].fromDatum(d) |> (abs => Map.fromIList(abs)) )

  implicit def DataServiceMode: Data[Service.Mode] =
    Enum[Service.Mode](
      "Service.Mode",
      List(Service.Mode.Test, Service.Mode.Staging, Service.Mode.Live),
      _.toLowerCase )

//======================================================================
  implicit def DataKey[A: Data: Ordered]: Data[Key[A]] = Struct(
    "Key",
    "secret" -> empty[Bits]   :+:
    "mode"   -> empty[Service.Mode]:+:
    "status" -> empty[Status] :+:
    "rights" -> empty[ACL[A]] :+:
    HNil )(
    (x: Key[A]) =>
    x.secret :+: x.mode :+: x.status :+: x.rights :+: HNil )(
    (x: Bits:+:Service.Mode:+:Status:+:ACL[A]:+:HNil) =>
    Key(x.i[_0], x.i[_1], x.i[_2], x.i[_3]) )

  implicit def DataStatus: Data[Status] =
    Enum[Status]("Status", List(Status.Active, Status.Passive), _.toLowerCase )

  implicit def DataACL[A: Data: Ordered]: Data[ACL[A]] = Struct(
    "ACL", "map" -> empty[Map[A, Mode]] :+: HNil )(
    (x: ACL[A]) => x.map :+: HNil )(
    (x: Map[A, Mode]:+:HNil) => ACL(x.i[_0]) )

  implicit def DataMode: Data[Mode] = Struct(
    "Mode",
    "read"    -> empty[Boolean] :+:
    "write"   -> empty[Boolean] :+:
    "execute" -> empty[Boolean] :+:
    HNil )(
    (x: Mode) => x.read :+: x.write :+: x.execute :+: HNil )(
    (x: Boolean:+:Boolean:+:Boolean:+:HNil) =>
    Mode(x.i[_0], x.i[_1], x.i[_2]) )

  implicit def DataPermission: Data[Permission] =
    Enum[Permission](
      "Permission",
      List(Permission.Read, Permission.Write, Permission.Execute),
      _.toLowerCase )

} // DataInstances
