// Transparently encrypt a subset of class fields.
package util
package sec

import prelude._, std._, data._

trait Encrypt[A] {
  def encrypt(a: A): Encrypted[A]
  def decrypt(a: Encrypted[A]): A
}
object Encrypt {
  def apply[A: Encrypt](implicit ev: Encrypt[A]) = ev
  def instance[A](enc: A => A, dec: A => A): Encrypt[A] = new Encrypt[A] {
    def encrypt(a: A)            = a |> enc |> Encrypted.Tag
    def decrypt(a: Encrypted[A]) = a |> Encrypted.Untag |> dec
  }
}

object Encrypted {
  type T[A] = A @@ Tag
  sealed trait Tag
  @inline private[sec] def Tag  [A](a: A): Encrypted[A] = NewType(a)
  @inline private[sec] def Untag[A](a: Encrypted[A]): A = NewType unwrap a

  def DataEncrypted[A: Data]: Data[Encrypted[A]] = Data.instance(
    ea => ea |> Untag |> Data[A].toDatum,
    d  => d |> Data[A].fromDatum |> Tag )

} //Encrypted
