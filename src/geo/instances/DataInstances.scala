package util
package geo
package instances

import prelude._, std._, data._
import typ.hlist._, typ.nat._
import std.instances.data._

trait DataInstances {

  implicit def DataIP: Data[IP] = Struct(
    "IP",
    "ip"        -> empty[String] :+:
    "version"   -> empty[String] :+:
    "country"   -> empty[String] :+:
    "region"    -> empty[String] :+:
    "city"      -> empty[String] :+:
    "zip"       -> empty[String] :+:
    "latitude"  -> empty[Double] :+:
    "longitude" -> empty[Double] :+:
    "isEU"      -> empty[Boolean] :+:
    HNil )(
    (x: IP) =>
     x.ip :+: x.version :+: x.country :+: x.region :+: x.city :+: x.zip :+:
     x.latitude :+: x.longitude :+: x.isEU :+: HNil )(
    (x: String:+:String:+:String:+:String:+:String:+:String:+:
       Double:+:Double:+:Boolean:+:HNil) =>
    IP(x.i[_0], x.i[_1], x.i[_2], x.i[_3], x.i[_4], x.i[_5],
       x.i[_6], x.i[_7], x.i[_8]) )

} //DataInstances
