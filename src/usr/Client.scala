package util
package usr

import prelude._, std._
import data.Data
import net._, dsl._, netty.crypto.Bits

import std.instances.data._
import usr.instances.data._
import sec.instances.data.{DataMap => _, _}

class Client
  [Entity: Data: Ordered, Settings: Data](prim: PrimCall)
  extends net.Client(prim)
{
  def register(email: String, pass: String) =
    Call [Body, Unit, Token] (Post upon Root / "user" / "register")
      .body(Map("email" -> email, "pass" -> pass))

  def login(email: String, pass: String) =
    Call [Body, Unit, Token] (Post upon Root / "user" / "login")
      .body(Map("email" -> email, "pass" -> pass))

  def authenticate(token: Token) =
    Call [Body, Unit, Bits -> List[Entity]] (
      Post upon Root / "user" / "authenticate")
      .body(Map("token" -> token.get))

  def logout(key: Bits) =
    Call [Unit, Unit, Unit] (Post upon Root / "user" / "logout")
      .header(AuthHeader(key))

  def requestVerify(key: Bits) =
    Call [Unit, Unit, Unit] (Post upon Root / "user" / "verify" / "request")
      .header(AuthHeader(key))

  def performVerify(token: Token) =
    Call [Body, Unit, Unit] (Post upon Root / "user" / "verify" / "perform")
      .body(Map("token" -> token.get))

  def requestReset(email: String) =
    Call [Body, Unit, Unit] (Post upon Root / "user" / "reset" / "request")
      .body(Map("email" -> email))

  def performReset(token: Token, pass: String) =
    Call [Body, Unit, Unit] (Post upon Root / "user" / "reset" / "perform")
      .body(Map("token" -> token.get, "pass" -> pass))

  def getUser(key: Bits) =
    Call [Unit, Unit, User[Entity, Settings]] (Get upon Root / "user" )
      .header(AuthHeader(key))

  def getProfile(key: Bits) =
    Call [Unit, Unit, Profile] (Get upon Root / "user" / "profile")
      .header(AuthHeader(key))

  def putProfile(key: Bits, profile: Profile) =
    Call [Profile, Unit, Profile] (Put upon Root / "user" / "profile")
      .header(AuthHeader(key))
      .body(profile)

  def getSettings(key: Bits) =
    Call [Unit, Unit, Settings] (Get upon Root / "user" / "settings")
      .header(AuthHeader(key))

  def putSettings(key: Bits, settings: Settings) =
    Call [Settings, Unit, Settings] (Put upon Root / "user" / "settings")
      .header(AuthHeader(key))
      .body(settings)

} //Client
