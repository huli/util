package util
package usr
package instances

import prelude._, std._, data._
import fsm.FID
import typ.hlist._, typ.nat._
import std.instances.data._
import data.instances.data._
import sec.instances.data._
import fsm.instances.data._
import mail.instances.data._
import net.netty.crypto.Bits

trait DataInstances {

  implicit def DataUser
    [Entity: Data: Ordered, Settings: Data]
    : Data[User[Entity, Settings]] = Struct(
    "User",
    "email"       -> empty[Email]               :+:
    "credentials" -> empty[Credentials[Entity]] :+:
    "sessions"    -> empty[List[Token]]         :+:
    "profile"     -> empty[Profile]             :+:
    "settings"    -> empty[Settings]            :+:
    "id"          -> empty[FID]                 :+:
    HNil )(
    (x: User[Entity, Settings]) =>
    x.email:+:x.credentials:+:x.sessions:+:x.profile:+:x.settings:+:x.id:+:HNil )(
    (x: Email:+:Credentials[Entity]:+:List[Token]:+:Profile:+:Settings:+:FID:+:HNil) =>
    User(x.i[_0], x.i[_1], x.i[_2], x.i[_3], x.i[_4], x.i[_5]) )

  implicit def DataEmail: Data[Email] = Struct(
    "Email",
    "address"   -> empty[Address]      :+:
    "token"     -> empty[Maybe[Token]] :+:
    "wantsMail" -> empty[Boolean]      :+:
    HNil )(
    (x: Email) =>
    x.address:+:x.token:+:x.wantsMail:+:HNil )(
    (x: Address:+:Maybe[Token]:+:Boolean:+:HNil) =>
    Email(x.i[_0], x.i[_1], x.i[_2]) )

  implicit def DataAddress: Data[Address] = Struct(
    "Address",
    "get" -> empty[String] :+: HNil )(
    (x: Address) => x.get:+:HNil )(
    (x: String:+:HNil) => new Address(x.i[_0]) )

  implicit def DataCredentials
    [Entity: Data: Ordered]: Data[Credentials[Entity]] = Struct(
    "Credentials",
    "pass"  -> empty[Pass]            :+:
    "token" -> empty[Maybe[Token]]    :+:
    "key"   -> empty[sec.Key[Entity]] :+:
    HNil )(
    (x: Credentials[Entity]) => x.pass:+:x.token:+:x.key:+:HNil )(
    (x: Pass:+:Maybe[Token]:+:sec.Key[Entity]:+:HNil) =>
    Credentials(x.i[_0], x.i[_1], x.i[_2]) )

  implicit def DataPass: Data[Pass] = Struct(
    "Pass",
    "get"   -> empty[String]       :+:
    "salt"  -> empty[String]       :+:
    HNil )(
    (x: Pass) =>
    x.get:+:x.salt:+:HNil )(
    (x: String:+:String:+:HNil) =>
    Pass(x.i[_0], x.i[_1]) )

  implicit def DataToken: Data[Token] = Struct(
    "Token",
    "get" -> empty[String] :+: HNil )(
    (x: Token) => x.get:+:HNil )(
    (x: String:+:HNil) => Token(x.i[_0]) )

  implicit def DataProfile: Data[Profile] = Struct(
    "Profile",
    "status"     -> empty[Status] :+:
    "username"   -> empty[Maybe[String]] :+:
    "name"       -> empty[Maybe[String]] :+:
    "bio"        -> empty[Maybe[String]] :+:
    "location"   -> empty[Maybe[String]] :+:
    "website"    -> empty[Maybe[String]] :+:
    "birthdate"  -> empty[Maybe[String]] :+:
    "following"  -> empty[List[FID]] :+:
    "followers"  -> empty[List[FID]] :+:
    "referredBy" -> empty[List[FID]] :+:
    "referred"   -> empty[List[FID]] :+:
    HNil )(
    (x: Profile) =>
    x.status:+:
    x.username:+:x.name:+:x.bio:+:x.location:+:x.website:+:x.birthdate:+:
    x.following:+:x.followers:+:x.referredBy:+:x.referred:+:HNil )(
    (x: Status:+:
        Maybe[String]:+:Maybe[String]:+:Maybe[String]:+:
        Maybe[String]:+:Maybe[String]:+:Maybe[String]:+:
        List[FID]:+:List[FID]:+:List[FID]:+:List[FID]:+:
        HNil) =>
    Profile(x.i[_0],
            x.i[_1], x.i[_2], x.i[_3], x.i[_4], x.i[_5], x.i[_6],
            x.i[_7], x.i[_8], x.i[_9], x.i[_10]) )

  implicit val DataStatus: Data[Status] =
    Enum[Status](
      "Status",
      List(Status.Dev, Status.User, Status.Customer, Status.Affiliate),
      _.toLowerCase)

//======================================================================
  import Message._

  implicit def DataRegister[E, S]: Data[Register[E, S]] = Struct(
    "Register",
    "login" -> empty[Address] :+:
    "pass"  -> empty[Pass]    :+:
    HNil )(
    (x: Register[E, S]) => x.l:+:x.p:+:HNil )(
    (x: Address:+:Pass:+:HNil) => Register(x.i[_0], x.i[_1]) )

  implicit def DataLogin[E, S]: Data[Login[E, S]] = Struct(
    "Login",
    HNil )(
    (x: Login[E, S]) => HNil )(
    (x: HNil) => Login() )

  implicit def DataLogout[E, S]: Data[Logout[E, S]] = Struct(
    "Logout",
    HNil )(
    (x: Logout[E, S]) => HNil )(
    (x: HNil) => Logout() )

  implicit def DataRequestVerify[E, S]: Data[RequestVerify[E, S]] = Struct(
    "RequestVerify",
    HNil )(
    (x: RequestVerify[E, S]) => HNil )(
    (x: HNil) => RequestVerify() )

  implicit def DataRequestReset[E, S]: Data[RequestReset[E, S]] = Struct(
    "RequestReset",
    HNil )(
    (x: RequestReset[E, S]) => HNil )(
    (x: HNil) => RequestReset() )

  implicit def DataPerformVerify[E, S]: Data[PerformVerify[E, S]] = Struct(
    "PerformVerify",
    "token" -> empty[Token] :+:
    HNil )(
    (x: PerformVerify[E, S]) => x.t:+:HNil )(
    (x: Token:+:HNil) => PerformVerify(x.i[_0]) )

  implicit def DataPerformReset[E, S]: Data[PerformReset[E, S]] = Struct(
    "PerformReset",
    "token" -> empty[Token] :+:
    "pass"  -> empty[Pass]  :+:
    HNil )(
    (x: PerformReset[E, S]) => x.t:+:x.p:+:HNil )(
    (x: Token:+:Pass:+:HNil) => PerformReset(x.i[_0], x.i[_1]) )

  implicit def DataPutProfile[E, S]: Data[PutProfile[E, S]] = Struct(
    "PutProfile",
    "get" -> empty[Profile] :+:
    HNil )(
    (x: PutProfile[E, S]) => x.get:+:HNil )(
    (x: Profile:+:HNil) => PutProfile(x.i[_0]) )

  implicit def DataPutSettings[E, S: Data]: Data[PutSettings[E, S]] = Struct(
    "PutSettings",
    "get" -> empty[S] :+:
    HNil )(
    (x: PutSettings[E, S]) => x.get:+:HNil )(
    (x: S:+:HNil) => PutSettings(x.i[_0]) )

  implicit def DataPutRights[E: Data: Ordered, S]: Data[PutRights[E, S]] = Struct(
    "PutRights",
    "get" -> empty[List[E]] :+:
    HNil )(
    (x: PutRights[E, S]) => x.get:+:HNil )(
    (x: List[E]:+:HNil) => PutRights(x.i[_0]) )

  implicit def DataDeleteRights
    [E: Data: Ordered, S]: Data[DeleteRights[E, S]] = Struct(
    "DeleteRights",
    "get" -> empty[List[E]] :+:
    HNil )(
    (x: DeleteRights[E, S]) => x.get:+:HNil )(
    (x: List[E]:+:HNil) => DeleteRights(x.i[_0]) )

  implicit def DataMessage[E: Data: Ordered, S: Data]: Data[Message[E, S]] =
    Union[Message[E, S],
          Register[E, S]      :+:
          Login[E, S]         :+:
          Logout[E, S]        :+:
          RequestVerify[E, S] :+:
          RequestReset[E, S]  :+:
          PerformVerify[E, S] :+:
          PerformReset[E, S]  :+:
          PutProfile[E, S]    :+:
          PutSettings[E, S]   :+:
          PutRights[E, S]     :+:
          DeleteRights[E, S]  :+:
          HNil]("Message")

//======================================================================
  import Action._

  implicit def DataIndexEmail[E: Data: Ordered, S: Data]: Data[IndexEmail[E, S]] = Struct(
    "IndexEmail",
    "key"   -> empty[Address] :+:
    "value" -> empty[FID]     :+:
    HNil )(
    (x: IndexEmail[E, S]) => x.key:+:x.value:+:HNil )(
    (x: Address:+:FID:+:HNil) => IndexEmail(x.i[_0], x.i[_1]) )

  implicit def DataIndexKey[E: Data: Ordered, S: Data]: Data[IndexKey[E, S]] = Struct(
    "IndexKey",
    "key"   -> empty[Bits] :+:
    "value" -> empty[FID]  :+:
    HNil )(
    (x: IndexKey[E, S]) => x.key:+:x.value:+:HNil )(
    (x: Bits:+:FID:+:HNil) => IndexKey(x.i[_0], x.i[_1]) )

  implicit def DataSend[E: Data: Ordered, S: Data]: Data[Send[E, S]] = Struct(
    "Send",
    "get" -> empty[mail.Mail] :+:
    HNil )(
    (x: Send[E, S]) => x.get:+:HNil )(
    (x: mail.Mail:+:HNil) => Send(x.i[_0]) )

  implicit def DataInit[E: Data: Ordered, S: Data]: Data[Init[E, S]] = Struct(
    "Init",
    "get" -> empty[User[E, S]] :+:
    HNil )(
    (x: Init[E, S]) => x.get:+:HNil )(
    (x: User[E, S]:+:HNil) => Init(x.i[_0]) )

  implicit def DataAction[E: Data: Ordered, S: Data]: Data[Action[E, S]] =
    Union[Action[E, S],
          IndexEmail[E, S]:+:IndexKey[E, S]:+:Send[E,S]:+:Init[E, S]:+:HNil](
      "Message")

} //DataInstances
