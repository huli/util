// Entity ACL is used to allow granular API access.
package util
package usr

import prelude._, std._
import fsm.FID
import std.instances.data._

final case class User
  [Entity: Ordered, Settings]
  (email      : Email,
   credentials: Credentials[Entity],
   sessions   : List[Token],
   profile    : Profile,  //user-specific data
   settings   : Settings, //application-specific data
   id         : FID = FID(""))
{
  def putRights(es: List[Entity]): User[Entity, Settings] = {
    val mode   = sec.Mode(execute=true) //XXX
    val rights = (credentials.key.rights /: es)(_.put(_, mode))
    copy(
      credentials = credentials.copy(
        key       = credentials.key.copy(
          rights  = rights ) ) )
  }

    def deleteRights(es: List[Entity]): User[Entity, Settings] = {
      val rights = (credentials.key.rights /: es)(_ delete _)
      copy(
        credentials = credentials.copy(
          key       = credentials.key.copy(
            rights  = rights ) ) )
    }
}

//======================================================================
final case class Email
  (address  : Address,
   token    : Maybe[Token], //empty indicates verified
   wantsMail: Boolean = true)

final case class Address(get: String) extends AnyVal

object Address {
  def apply(s: String): Maybe[Address] =
    s |> canonicalize |> check map (new Address(_))

  def canonicalize(s: String) = s.trim.toLowerCase

  def check(s: String): Maybe[String] =
    """^[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\.)+[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?$""".r.findFirstIn(s).fold(empty[String])(_.just)
}

//======================================================================
final case class Credentials
  [Entity: Ordered]
  (pass : Pass,
   token: Maybe[Token], //non-empty if a reset has been requested
   key  : sec.Key[Entity])

final case class Pass(get: String, salt: String)
{
  def check(raw: String): Boolean = get == sec.crypto.scrypt(raw, salt)
}

object Pass {
  def apply(raw: String): Pass =
    sec.crypto.scrypt(raw) match {
      case (hashed, salt) => Pass(hashed, salt) }
}

//======================================================================
final case class Token(get: String) extends AnyVal

object Token {
  def apply(cipher: sec.Cipher)(id: FID, expires: Timestamp): Token = {
    val randomness = sec.crypto.salt()
    val str        = s"${id.get}${SEP}${expires.get}${SEP}${randomness}"
    Token(cipher encrypt str) }

  def parse(cipher: sec.Cipher)(t: Token): Maybe[FID] =
    cipher decrypt t.get split SEP match {
      case Array(id, expires, _)
          if Timestamp.now before Timestamp(expires.toLong) =>
        FID(id).just
      case _ => empty }

  private val SEP = "__"
}

//======================================================================
final case class Profile
  (status    : Status        = Status.User,
   username  : Maybe[String] = empty,
   name      : Maybe[String] = empty,
   bio       : Maybe[String] = empty,
   location  : Maybe[String] = empty,
   website   : Maybe[String] = empty,
   birthdate : Maybe[String] = empty,
   following : List[FID]     = nil,
   followers : List[FID]     = nil,
   referredBy: List[FID]     = nil,
   referred  : List[FID]     = nil)

sealed trait Status
object Status {
  case object Dev       extends Status
  case object User      extends Status
  case object Customer  extends Status
  // TODO: former customer?
  case object Affiliate extends Status
}

// User
