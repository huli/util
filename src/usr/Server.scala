package util
package usr

import prelude._, std._
import data.Data
import fsm.FID
import net.{ Request, Response }, net.dsl._, net.netty.crypto.Bits

import sec.Key

import std.instances.data._
import fsm.instances.data.DataFID

import sec.instances.data.{ DataMap => _, _ }
import usr.instances.data._

class Server
  [Entity: Data: Ordered: ClassTag, Settings: Data: ClassTag]
  (users  : FSM.Engine[Entity, Settings],
   context: Context[Entity, Settings])
  extends net.Server
{
  type Ref = FSM.Ref[Entity, Settings]

//======================================================================
  val handle =
    Handle [Body, Unit, Token] {
      case Post upon Root / "user" / "register"           => register
      case Post upon Root / "user" / "login"              => login        }|(

    Handle [Body, Unit, Bits -> List[Entity]] {
      case Post upon Root / "user" / "authenticate"       => authenticate })|(

    Handle [Unit, Unit, Unit] {
      case Post upon Root / "user" / "logout"             => logout
      case Post upon Root / "user" / "verify" / "request" => requestVerify })|(

    Handle [Body, Unit, Unit] {
      case Post upon Root / "user" / "verify" / "perform" => performVerify
      case Post upon Root / "user" / "reset"  / "request" => requestReset
      case Post upon Root / "user" / "reset"  / "perform" => performReset })|(

    Handle [Unit, Unit, User[Entity, Settings]] {
      case Get  upon Root / "user"                        => getUser      })|(

    Handle [Unit, Unit, Profile] {
      case Get  upon Root / "user" / "profile"            => getProfile   })|(

    Handle [Profile, Unit, Profile] {
      case Put  upon Root / "user" / "profile"            => putProfile   })|(

    Handle [Unit, Unit, Settings] {
      case Get  upon Root / "user" / "settings"           => getSettings  })|(

    Handle [Settings, Unit, Settings] {
      case Put  upon Root / "user" / "settings"           => putSettings  })

//======================================================================
  type Parse[EID]   = Request[_] => Maybe[EID]
  type Certify[EID] = EID        => Task[Maybe[FID]]

  def Handler
    [A: ClassTag: Data, B: Data, EID]
    (parse  : Parse[EID],
     certify: Certify[EID],
     handle : (EID, Maybe[Ref], A) => Task[B])
    : Request[A] => Task[Response[Unit, B]]
    = req => (parse(req), body(req)) match {
      case (Just(eid), Just(a)) =>
        certify(eid)                map
        (_ map users.unsafeCertify) flatMap
        (handle(eid, _, a))         fold (
          b =>  Response.ok[Unit, B](b),
          { case exn if exn.cause == data.DString("pass") =>
            Response.notAuthorized(unit)
            case exn => throw exn } )
      case _ =>
        Task0(Response.error[Unit, B](unit)) & Entry.nop }

  // FIXME
  def get(s: String) = (_: Request[_]).body.flatMap(_.asInstanceOf[Body] lookup s)

  val ParseAddress  : Parse[Address]   = get("email")(_) flatMap (Address(_))
  val ParseToken    : Parse[Token]     = get("token")(_) map     (Token(_))
  val ParseKey      : Parse[Bits]      = getAuthHeader(_)
  val CertifyAddress: Certify[Address] = context.emailIdx lookup _
  val CertifyToken  : Certify[Token]   = context.parse(_) |> (Task0(_) ~> Log.info("token.fid"))
  val CertifyKey    : Certify[Bits]    = context.keyIdx lookup _

//======================================================================
// N.B. must be careful not to leak plaintext passwords into logs.

  // Exchange password for session token
  val register = Handler[Body, Token, Address](
    ParseAddress,
    CertifyAddress,
    (addr, mref, body) => for {
      _   <- Task.fail("Server.register", addr) if !mref.isEmpty
      pass = body.lookup("pass").map(Pass(_))
      _   <- Task.fail("Server.register", body) if pass.isEmpty
      ref <- users.create
      s   <- ref ! Message.Register[Entity, Settings](addr, unsafe.unwrap(pass))
    } yield s.sessions.headMaybe |> unsafe.unwrap )

  val login = Handler[Body, Token, Address](
    ParseAddress,
    CertifyAddress,
    (addr, mref, body) => for {
      _   <- Task.fail("Server.login", addr) if mref.isEmpty
      pass = body.lookup("pass")
      _   <- Task.fail("Server.login", "body") if pass.isEmpty
      raw  = unsafe.unwrap(pass)
      s   <- unsafe.unwrap(mref).?
      _   <- Task.fail("Server.login", "pass") if !(s.credentials.pass check raw)
      s   <- unsafe.unwrap(mref) ! Message.Login[Entity, Settings]()
    } yield s.sessions.headMaybe |> unsafe.unwrap )

  // Exchange session token for API key.
  val authenticate = Handler[Body, Bits -> List[Entity], Token](
    ParseToken,
    CertifyToken,
    (token, mref, _body) => for {
      _ <- Task.fail("Server.authenticate", token) if mref.isEmpty
      s <- unsafe.unwrap(mref).?
      _ <- Task.fail("Server.authenticate", "expired") if s.sessions != List(token)
    } yield {
      val bits     = s.credentials.key.secret
      val acl      = s.credentials.key.rights
      val entities = acl.map.toIList filter {
        case entity -> mode => mode.execute //XXX
      } map fst
      bits -> entities
    } )

  // Clear session tokens.
  val logout = Handler[Unit, Unit, Bits](
    ParseKey,
    CertifyKey,
    (_bits, mref, _) => for {
      _ <- Task.fail("Server.logout", unit) if mref.isEmpty
      _ <- unsafe.unwrap(mref) ! Message.Logout[Entity, Settings]()
    } yield unit )

//======================================================================
  val requestVerify = Handler[Unit, Unit, Bits](
    ParseKey,
    CertifyKey,
    (_bits, mref, _) => for {
      _ <- Task.fail("Server.requestVerify", unit) if mref.isEmpty
      _ <- unsafe.unwrap(mref) ! Message.RequestVerify[Entity, Settings]()
    } yield unit )

  val performVerify = Handler[Body, Unit, Token](
    ParseToken,
    CertifyToken,
    (token, mref, _) => for {
      _ <- Task.fail("Server.performVerify", unit) if mref.isEmpty
      s <- unsafe.unwrap(mref) ! Message.PerformVerify[Entity, Settings](token)
      _ <- Task.fail("Server.performVerify", token) if !s.email.token.isEmpty
    } yield unit )

  val requestReset = Handler[Body, Unit, Address](
    ParseAddress,
    CertifyAddress,
    (addr, mref, _body) => for {
      _ <- Task.fail("Server.requestReset", unit) if mref.isEmpty
      _ <- unsafe.unwrap(mref) ! Message.RequestReset[Entity, Settings]()
    } yield unit )

  val performReset = Handler[Body, Unit, Token](
    ParseToken,
    CertifyToken,
    (token, mref, body) => for {
      _   <- Task.fail("Server.performReset", token) if mref.isEmpty
      pass = body.lookup("pass").map(Pass(_))
      _   <- Task.fail("Server.performReset", "body") if pass.isEmpty
      s   <- unsafe.unwrap(mref) ! Message.PerformReset[Entity, Settings](
        token, unsafe.unwrap(pass))
      _ <- Task.fail("Server.performReset", token) if !s.credentials.token.isEmpty
    } yield unit )

//======================================================================
  val getUser = Handler[Unit, User[Entity, Settings], Bits](
    ParseKey, CertifyKey,
    (_bits, mref, _) => unsafe.unwrap(mref).? )

  val getProfile = Handler[Unit, Profile, Bits](
    ParseKey, CertifyKey,
    (_bits, mref, _) => unsafe.unwrap(mref).? map (_.profile) )

  val putProfile = Handler[Profile, Profile, Bits](
    ParseKey, CertifyKey,
    (_bits, mref, profile) => for {
      s <- unsafe.unwrap(mref) ! Message.PutProfile[Entity, Settings](profile)
    } yield s.profile )

  val getSettings = Handler[Unit, Settings, Bits](
    ParseKey, CertifyKey,
    (_bits, mref, _) => unsafe.unwrap(mref).? map (_.settings) )

  val putSettings = Handler[Settings, Settings, Bits](
    ParseKey, CertifyKey,
    (_bits, mref, settings) => for {
      s <- unsafe.unwrap(mref) ! Message.PutSettings[Entity, Settings](settings)
    } yield s.settings )

} //Server
