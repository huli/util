package util
package usr

import prelude._, std._
import data.Data
import fsm.{ Description0, FID, Msg }
import net.netty.crypto.Bits

import usr.instances.data._

class FSM
  [Entity: Data: Ordered, Settings: Data](context: Context[Entity, Settings])
  extends Description0[User[Entity, Settings], Message[Entity, Settings], Action[Entity, Settings]]
{
  def initialState = User(
    Email(new Address(""), Just(Token(""))),
    Credentials(Pass(""), empty, sec.Key.unpadded()), //!!!
    nil,
    Profile(),
    context.defaults )

//======================================================================
  def transitions0(self: FID) = {
    case user0 -> Message.Register(login, pass) =>
      val user1     = user0.copy(
        email       = Email(login, context.VerifyToken(self).just),
        credentials = user0.credentials.copy(pass=pass),
        sessions    = List(context.SessionToken(self)) )
      val user2     = user1.putRights(context.basicRights)
      val user      = user2.copy(id=self)
      val actions   = List[Action[Entity, Settings]](
        Action.Send(context.welcomeMail(user.email.address,
                                        user.email.token |> unsafe.unwrap)),
        Action.IndexEmail(user.email.address, self),
        Action.IndexKey(user.credentials.key.secret, self),
        Action.Init(user))
      user -> actions

    case user -> Message.Login() =>
      user.copy(sessions = List(context.SessionToken(self))) -> nil

    case user -> Message.Logout() =>
      user.copy(sessions = nil) -> nil

    case user0 -> Message.RequestVerify() =>
      // Do nothing if already verified.
      val token   = user0.email.token.cata(_ =>
        context.VerifyToken(self).just, //generate fresh/not expired
        empty[Token] )
      val user    = user0.copy(email = user0.email.copy(token = token))
      val actions = token.cata(
        tok => List[Action[Entity, Settings]](
          Action.Send(context.verifyMail(user.email.address, tok))),
        nil[Action[Entity, Settings]] )
      user -> actions

    case user -> Message.PerformVerify(t) =>
      val token0 = user.email.token
      val token  = if (token0 == Just(t)) empty[Token] else token0
      user.copy(email = user.email.copy(token = token)) -> nil

    case user0 -> Message.RequestReset() =>
      val token     = context.ResetToken(self)
      val user      = user0.copy(
        credentials = user0.credentials.copy(
          token     = token.just ) )
      val actions   = List[Action[Entity, Settings]](
        Action.Send(context.resetMail(user.email.address, token)))
      user -> actions

    case user -> Message.PerformReset(token, newPass) =>
      val credentials =
        if (user.credentials.token == Just(token))
          user.credentials.copy(token=empty, pass=newPass)
        else
          user.credentials
      // TODO: clear session?
      user.copy(credentials=credentials) -> nil

    case user -> Message.PutProfile(profile) =>
      user.copy(profile = profile) -> nil

    case user -> Message.PutSettings(settings) =>
      user.copy(settings = settings) -> nil

    case user -> Message.PutRights(entities) =>
      user.putRights(entities) -> nil

    case user -> Message.DeleteRights(entities) =>
      user.deleteRights(entities) -> nil
  }

//======================================================================
  override def actions0(self: FID) = {
    case Msg(Action.Init(user), _)       => context.init(self, user) }
  def actions(self: FID) = {
    case Msg(Action.IndexEmail(k, v), _) => context.emailIdx insert (k, v)
    case Msg(Action.IndexKey(k, v), _)   => context.keyIdx   insert (k, v)
    case Msg(Action.Send(m), _)          => context.smtpd send m
    case Msg(Action.Init(_), _)          => unsafe.??? }
}

//======================================================================
object FSM {
  type Ref[E, S] = fsm.FSMRef[User[E, S], Message[E, S], Action[E, S]]
  type T[E, S]   = fsm.FSM[User[E, S], Message[E, S], Action[E, S]]

  class Engine
    [E: Data: Ordered, S: Data]
    (context: Context[E, S],
     store  : fsm.Store[User[E, S], Message[E, S], Action[E, S]])
    extends fsm.Engine(
    "users",
    new FSM[E, S](context),
    fsm.Policy.Default,
    store,
    fsm.Protocol.Sync[User[E, S], Message[E, S], Action[E, S]] ) //!!!
}

//======================================================================
sealed trait Message[+E, +S]
object Message {
  final case class Register     [E, S](l: Address, p: Pass) extends Message[E, S]
  final case class Login        [E, S]()                    extends Message[E, S]
  final case class Logout       [E, S]()                    extends Message[E, S]
  final case class RequestVerify[E, S]()                    extends Message[E, S]
  final case class PerformVerify[E, S](t: Token)            extends Message[E, S]
  final case class RequestReset [E, S]()                    extends Message[E, S]
  final case class PerformReset [E, S](t: Token, p: Pass)   extends Message[E, S]
  final case class PutProfile   [E, S](get: Profile)        extends Message[E, S]
  final case class PutSettings  [E, S](get: S)              extends Message[E, S]
  final case class PutRights    [E, S](get: List[E])        extends Message[E, S]
  final case class DeleteRights [E, S](get: List[E])        extends Message[E, S]

  def putRights   [E](es: List[E]): Message[E, Nothing] = PutRights(es)
  def deleteRights[E](es: List[E]): Message[E, Nothing] = DeleteRights(es)
}

sealed trait Action[+E, +S]
object Action {
  final case class IndexEmail[E, S](key: Address, value: FID) extends Action[E, S]
  final case class IndexKey  [E, S](key: Bits,    value: FID) extends Action[E, S]
  final case class Send      [E, S](get: mail.Mail)           extends Action[E, S]
  final case class Init      [E, S](get: User[E, S])          extends Action[E, S]
}

// FSM
