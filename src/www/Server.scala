// Static file serving.
package util
package www

import prelude._, std._
import data.Charset.Utf8, data.format.Text
import net.{ Code, Header, Request, Response }, net.dsl._

import std.instances.data._

class Server(dir: io.Fragment[String, String], root: String) extends net.Server
{
  override val content = Header.Content(Text(Utf8))

  val handle = Handle [String, String, String] {
    case Get upon Root => get(root)
    case Get upon path => get(path.toString) }

  def get(path: String)(req: Request[String]): Task[Response[String, String]] =
    dir.lookup(path) map {
      case Empty()      => Response(Code.NotFound())
      case Just(string) => Response.ok(string) }

} //Server
